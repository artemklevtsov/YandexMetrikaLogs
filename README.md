
<!-- README.md is generated from README.Rmd. Please edit that file -->

[![Build
status](https://gitlab.com/artemklevtsov/yametrika.logs/badges/master/pipeline.svg)](https://gitlab.com/artemklevtsov/yametrika.logs/commits/master)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![CRAN
status](https://www.r-pkg.org/badges/version/yametrika.logs)](https://cran.r-project.org/package=yametrika.logs)

# yametrika.logs

## Installation

To install the development version the following command can be
used:

``` r
remotes::install_gitlab("artemklevtsov/yametrika.logs")
```

## Usage

```r
# Load package
library(yametrika.logs)
# Path to config file
path <- "/etc/yalogs"
# Create direcotry if not exists
dir.create(path, showWarnings = FALSE)
# Init config
init(path)
# Edit config
# Load config
load_config(file.path(path, "config.yml"))
# DB prepare
db_prepare()
# Update goals
update_goals(file.path(path, "config.yml"))
# Make request to prepare logs
request(file.path(path, "config.yml"))
# Fetch and upload preparted request
upload(file.path(path, "config.yml"))
```

## Server deploy

Copy config template to separate directory:

```bash
# Target config direcotry
CONFDIR=/etc/yalogs
# Package assets direcotry
PKGDIR=$(Rscript -e 'cat(system.file(package = "yametrika.logs"))')
# Create config directory
sudo mkdir -p  ${CONFDIR}
# Copy config template file
sudo Rscript -e 'yametrika.logs::init(commandArgs(TRUE))' ${CONFDIR}
# Copy systemd-units
sudo cp ${PKGDIR}/systemd/* /etc/systemd/system/
```

Manual edit required:

- `/etc/yalogs/config.yml` - define database credentials and desired fields fomr Yandex logs.
- `/etc/systemd/system/ym-goals.service` - replace `${CONFPATH}` with relevant path.
- `/etc/systemd/system/ym-log-request.service` - replace `${CONFPATH}` with relevant path.
- `/etc/systemd/system/ym-log-upload.service` - replace `${CONFPATH}` with relevant path.

You can use `sed` to edit unit files:

```bash
CONFPATH=/etc/yalogs/config.yml
sudo sed "s|\${CONFPATH}|${CONFPATH}|g" -i /etc/systemd/system/ym-*.service
```

Enable and start systemd timers:

```bash
sudo systemctl enable ym-log-request.timer
sudo systemctl start ym-log-request.timer
sudo systemctl enable ym-log-upload.timer
sudo systemctl start ym-log-upload.timer
sudo systemctl enable ym-goals.timer
sudo systemctl start ym-goals.timer
systemctl list-timers
```

To monitor the services:

```bash
journalctl -u ym-goals.service -o cat
journalctl -u ym-log-request.service -o cat
journalctl -u ym-log-upload.service -o cat
```


## Bug reports

Use the following command to go to the page for bug report submissions:

``` r
bug.report(package = "yametrika.logs")
```

Before reporting a bug or submitting an issue, please do the following:

  - Make sure that no error was found and corrected previously
    identified. You can use the search by the bug tracker;
  - Check the news list for the current version of the package. An error
    it might have been caused by changes in the package. This can be
    done with `news(package = "yametrika.logs", Version ==
    packageVersion("yametrika.logs"))` command;
  - Make a minimal reproducible example of the code that consistently
    causes the error;
  - Make sure that the error triggered in the function from the
    `yametrika.logs` package, rather than in the code that you pass,
    that is other functions or packages;
  - Try to reproduce the error with the last development version of the
    package from the git repository.

When submitting a bug report please include the output produced by
functions `traceback()` and `sessionInfo()`. This may save a lot of
time.

## License

The `yametrika.logs` package is distributed under [Apache
License 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.
