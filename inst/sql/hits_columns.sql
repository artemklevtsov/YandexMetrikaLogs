ALTER TABLE "{schema}"."{prefix}hits" ADD COLUMN client_uuid uuid;
ALTER TABLE "{schema}"."{prefix}hits" ADD COLUMN watch_uuid uuid;
ALTER TABLE "{schema}"."{prefix}hits" ADD COLUMN visit_uuid uuid; 
