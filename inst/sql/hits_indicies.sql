CREATE INDEX "{prefix}hits_client_uuid_idx"
          ON "{schema}"."{prefix}hits" (client_uuid);
CREATE INDEX "{prefix}hits_visit_uuid_idx"
          ON "{schema}"."{prefix}hits" (visit_uuid);
CREATE INDEX "{prefix}hits_date_time_idx"
          ON "{schema}"."{prefix}hits" (date_time);
CREATE INDEX "{prefix}hits_client_uuid_date_time_idx"
          ON "{schema}"."{prefix}hits" (client_uuid, date_time);
CREATE INDEX "{prefix}hits_visit_uuid_date_time_idx"
          ON "{schema}"."{prefix}hits" (visit_uuid, date_time);  
