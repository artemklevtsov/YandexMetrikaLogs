CREATE OR REPLACE FUNCTION "{schema}"."gen_{prefix}hits_uuids"()
RETURNS TRIGGER AS $$
BEGIN
    IF NEW.client_id IS NOT NULL AND NEW.client_uuid IS NULL THEN
       NEW.client_uuid := md5(NEW.client_id::text)::uuid;
    END IF;
    IF NEW.watch_id IS NOT NULL AND NEW.watch_uuid IS NULL THEN
       NEW.watch_uuid := md5(NEW.watch_id::text)::uuid;
    END IF;
    RETURN new;
END
$$ LANGUAGE plpgsql;

 CREATE TRIGGER "insert_uuids"
 BEFORE INSERT
     ON "{schema}"."{prefix}hits" FOR EACH ROW
EXECUTE PROCEDURE "{schema}"."gen_{prefix}hits_uuids"();
