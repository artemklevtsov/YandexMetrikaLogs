CREATE TABLE IF NOT EXISTS "{schema}"."{prefix}device_category" (
  id int2 PRIMARY KEY,
  description text UNIQUE
);

INSERT INTO "{schema}"."{prefix}device_category" (id, description)
VALUES
  (0, 'Undefined'),
  (1, 'Desktop'),
  (2, 'Mobile Phone'),
  (3, 'Tablet'),
  (4, 'TV')
ON CONFLICT (id) DO UPDATE SET description = EXCLUDED.description;


CREATE TABLE IF NOT EXISTS "{schema}"."{prefix}screen_orientation" (
  id int2 PRIMARY KEY,
  description text UNIQUE
);

INSERT INTO "{schema}"."{prefix}screen_orientation" (id, description)
VALUES
  (0, 'Undefined'),
  (1, 'Portrait'),
  (2, 'Landscape')
ON CONFLICT (id) DO UPDATE SET description = EXCLUDED.description;
