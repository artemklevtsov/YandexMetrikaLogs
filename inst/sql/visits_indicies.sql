CREATE INDEX "{prefix}visits_client_uuid_idx"
          ON "{schema}"."{prefix}visits" (client_uuid);
CREATE INDEX "{prefix}visits_date_time_idx"
          ON "{schema}"."{prefix}visits" (date_time);
CREATE INDEX "{prefix}visits_client_uuid_date_time_idx"
          ON "{schema}"."{prefix}visits" (client_uuid, date_time);
